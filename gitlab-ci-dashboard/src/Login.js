import React, { useState } from 'react';
import './styles.css'; // Import CSS file for styling

const Login = () => {
  const [projectPath, setProjectPath] = useState('');
  const [privateToken, setPrivateToken] = useState('');
  const [isFormValid, setIsFormValid] = useState(false);

  const handleInputChange = (e) => {
    const { name, value } = e.target;
    if (name === 'project-path') {
      setProjectPath(value);
    } else if (name === 'private-token') {
      setPrivateToken(value);
    }
    setIsFormValid(projectPath.trim() !== '' && privateToken.trim() !== '');
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    onConnect(projectPath, privateToken);
  };

  const onConnect = async (projectPath, privateToken) => {
    try {
      // Construct the request URL
      const apiUrl = projectPath + '?private_token=' + privateToken; // Update with your GitLab API endpoint

      // Send the request to GitLab
      const response = await fetch(apiUrl, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          // Add any other required headers here
        },
      });
  
      // Check if the request was successful
      if (response.ok) {
        // Handle successful login
        console.log('Login successful. Redirecting to dashboard...');
        // Redirect to the dashboard or perform any other action
      } else {
        // Handle login failure
        console.error('Login failed:', response.statusText);
        alert('Login failed. Please check your credentials.');
      }
    } catch (error) {
      // Handle errors
      console.error('An error occurred while connecting to GitLab:', error);
      alert('An error occurred while connecting to GitLab. Please try again later.');
    }
  };
  

  return (
    <div className="login-container">
      <h2>Login to GitLab</h2>
      <form onSubmit={handleSubmit}>
        <div className="form-group">
          <label htmlFor="project-path">Project Path:</label>
          <input
            type="text"
            id="project-path"
            name="project-path"
            value={projectPath}
            onChange={handleInputChange}
            required
          />
        </div>
        <div className="form-group">
          <label htmlFor="private-token">Private Token:</label>
          <input
            type="password"
            id="private-token"
            name="private-token"
            value={privateToken}
            onChange={handleInputChange}
            required
          />
        </div>
        <button type="submit" id="connect-btn" disabled={!isFormValid}>
          Connect
        </button>
      </form>
    </div>
  );
};

export default Login;
