import React from 'react';

const Connect = ({ onLogin }) => {
  const handleLogin = async () => {
    try {
      // Perform login/authentication here
      // Assuming you have a function to handle login in the parent component
      await onLogin();
    } catch (error) {
      console.error('Error logging in:', error);
      alert('An error occurred while logging in. Please try again later.');
    }
  };

  return (
    <div>
      <button onClick={handleLogin}>Connect</button>
    </div>
  );
};

export default Connect;
